import argparse
import select
import socket
import sys


def game_client(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)

    # connect to remote host
    try:
        s.connect((host, port))
    except:
        print('Unable to connect')
        sys.exit()

    print 'Te uniste a la sala'
    sys.stdout.write('[-] ')
    sys.stdout.flush()

    while 1:
        socket_list = [sys.stdin, s]

        # Get the list sockets which are readable
        ready_to_read, ready_to_write, in_error = select.select(socket_list, [], [])

        for sock in ready_to_read:
            if sock == s:
                # incoming message from remote server, s
                data = sock.recv(4096)
                if not data:
                    print '\nDesconectado de la sala de juego'
                    sys.exit()
                else:
                    # print data
                    sys.stdout.write(data)
                    sys.stdout.write('[-] ')
                    sys.stdout.flush()

            else:
                # user entered a message
                msg = sys.stdin.readline()
                s.send(msg)
                sys.stdout.write('[-] ')
                sys.stdout.flush()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Cliente de la Agenda")
    parser.add_argument('--port', help="puerto", type=int)
    parser.add_argument('--host', help="host", type=str)

    args = parser.parse_args()

    port = 9009
    host = ""

    if args.port:
        port = args.port

    if args.host:
        host = args.host
    game_client(host, port)
