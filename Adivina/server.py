#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import random
import select
import socket

RECV_BUFFER = 4096

MAX_PLAYERS = 3

NUMERO_DE_INTENTOS = 5


class GameServer():
    def __init__(self, HOST, PORT):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind((HOST, PORT))
        self.server_socket.listen(10)
        self.players = None
        self.SOCKET_LIST = []
        self.ganaste = False
        self.jugando = False

        self.current_player = None

        self.SOCKET_LIST.append(self.server_socket)
        print("El juego inicio en el puerto {port}".format(port=port))

        self.counter = 0
        self.intentos = 0

        self.number = random.randint(1, 20)

    def refrescar(self):
        self.number = random.randint(1, 20)
        self.ganaste = False
        self.jugando = True
        self.intentos = 0

    def cambiar_de_jugador(self):
        self.counter = (self.counter +1) % (MAX_PLAYERS-1)
        self.current_player = self.players[self.counter]

    def run(self):
        while True:
            ready_to_read, ready_to_write, in_error = select.select(self.SOCKET_LIST, [], [], 0)
            for sock in ready_to_read:
                if sock == self.server_socket:
                    sockfd, addr = self.server_socket.accept()
                    if len(self.SOCKET_LIST) >= MAX_PLAYERS:
                        sockfd.send("Ya no hay espacio en la sala de juego,"
                                    " el número máximo de participantes es 2,"
                                    "suerte para la próxima\n")
                        sockfd.close()
                    else:
                        self.SOCKET_LIST.append(sockfd)
                        print("Jugador  (%s, %s) connectado" % addr)
                        sockfd.send("Tú eres el jugador {}\n".format(self.SOCKET_LIST.index(sockfd)))
                        self.broadcast(sockfd, "El jugador {number} ingreso a la sala de juego\n"
                                       .format(number=self.SOCKET_LIST.index(sockfd)))

                        if len(self.SOCKET_LIST) == MAX_PLAYERS:
                            self.broadcast(message="El juego ha iniciado\n")
                            self.jugando = True
                            self.players = (self.SOCKET_LIST[1], self.SOCKET_LIST[2])
                            self.current_player = self.players[self.counter]
                            self.broadcast(message="Inicia el jugador 1\n")
                            self.current_player.send("Ingrese un número entre desde 1 hasta 20\n")
                else:
                    try:
                        data = sock.recv(RECV_BUFFER).strip()
                        if data:
                            if self.current_player == sock:
                                if unicode(data, "utf-8").isnumeric():
                                    val = int(data)
                                    if 0 <= val <= 20:

                                        self.broadcast(message="El jugador {} ingreso {}\n"
                                                       .format(self.counter + 1, val))

                                        if val < self.number:
                                            self.broadcast(message="El número es más alto\n")
                                            self.cambiar_de_jugador()
                                        elif val > self.number:
                                            self.broadcast(message="El número es más bajo\n")
                                            self.cambiar_de_jugador()
                                        else:
                                            self.broadcast(message="El jugador {} es un genio!!!\n"
                                                           .format(self.counter+1))
                                            self.ganaste = True
                                            self.jugando = False

                                        self.intentos += 1

                                        if not self.ganaste and self.intentos == NUMERO_DE_INTENTOS:
                                            self.broadcast(message="Han perdido, sera en otra oportunidad...\n")
                                            self.jugando = False

                                        if not self.jugando:
                                            self.refrescar()

                                    else:
                                        sock.send("Ingrese un número entre 0 y 20, reintentelo\n")
                                else:
                                    sock.send("Ingrese solo número, reintentelo\n")
                            else:
                                sock.send("Espere su turno\n")
                        else:
                            if sock in self.SOCKET_LIST:
                                self.SOCKET_LIST.remove(sock)
                            self.broadcast(sock=sock, message="El jugador (%s, %s) se desconecto\n" % addr)
                            print("(%s, %s) se desconecto" % addr)
                    except socket.error, exc:
                        self.broadcast(sock=sock, message="El jugador (%s, %s) se desconecto\n" % addr)
                        print("(%s, %s) se desconecto" % addr)
                        continue
        server_socket.close()

    def broadcast(self, sock=None, message=""):
        for socket in self.SOCKET_LIST:
            if socket != self.server_socket and socket != sock:
                try:
                    socket.send(message)
                except:
                    socket.close()
                    if socket in self.SOCKET_LIST:
                        self.SOCKET_LIST.remove(socket)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Cliente de la Agenda")
    parser.add_argument('--port', help="puerto", type=int)
    parser.add_argument('--host', help="host", type=str)

    args = parser.parse_args()

    port = 9009
    host = ""

    if args.port:
        port = args.port

    if args.host:
        host = args.host
    game = GameServer(host, port)
    game.run()
