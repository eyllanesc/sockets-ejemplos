#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import ast
import json
import socket


def menu():
    choice = None
    while not choice:
        msj = """
            1.Ingresar datos
            2.Ver datos
            3.Salir
            """

        print(msj)

        choice = raw_input('Ingresa una opción [1-3] : ')
        if choice.isdigit():
            if choice in ["1", "2", "3"]:
                return choice
        print("Ingrese una opción correcta por favor")
        choice = None


def send_request(host, port, msj):
    response = ""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect((host, port))
        sock.send(msj)
        response = sock.recv(1024)
    except:
        print("Hubo problemas con el servidor")
    finally:
        sock.close()
        return response


parser = argparse.ArgumentParser(description="Cliente de la Agenda")
parser.add_argument('--port', help="puerto", type=int)
parser.add_argument('--host', help="host", type=str)

args = parser.parse_args()

port = 8888
host = '127.0.0.1'

if args.port:
    port = args.port

if args.host:
    host = args.host

while True:

    option = menu()

    if option == "1":

        data = {"command": "write"}
        d = dict()
        d['name'] = raw_input("Ingrese su nombre: ")
        d['phone'] = raw_input("Ingres su teléfono: ")
        d['address'] = raw_input("Ingrese su dirección: ")
        data["info"] = d
        data = str(json.dumps(data))
        response = send_request(host, port, data)
        success = json.loads(response)['info']
        print("Se guardo los siguiente datos:\n"
              "Nombre: {name}, Teléfono: {phone} y Dirección: {address}".format(**success))

    elif option == "2":
        data = {"command": "read"}
        data = str(json.dumps(data))
        response = send_request(host, port, data)

        if response != " " and response != "":

            title = """
                     Directorio
                   ===============
                    """
            print(title)
            print(("{:>15}"*3).format("Nombres", "Teléfono", "Dirección"))

            for line in response.split("\n"):
                d = ast.literal_eval(line.replace("u'", "'").strip())
                print("{name:>15}{phone:>15}{address:>15}".format(**d))

        else:
            print("No existen datos")

    elif option == "3":
        print("Adios")
        break
