#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import json
import socket
import threading


class ThreadedServer(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        self.filename = 'data.txt'

    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            client.settimeout(60)
            threading.Thread(target=self.listen_to_client, args=(client, address)).start()

    def listen_to_client(self, client, address):
        size = 1024

        while True:
            try:
                data = client.recv(size)
                if data:
                    obj = json.loads(data)
                    cmd = str(obj["command"])
                    print("Se conecto: ", address, cmd)
                    if cmd == "write":
                        f = open(self.filename, "a+")
                        f.write(str(obj["info"])+"\n")
                        f.flush()
                        f.close()
                        client.send(data)
                    elif cmd == "read":
                        f = open(self.filename, "r")
                        data = f.read()
                        if data:
                            data = data.strip()
                        else:
                            data = " "
                        f.close()
                        client.send(data)

                else:
                    raise socket.error('Cliente desconectado')
            except:
                print("Se desconecto", address)
                client.close()
                return False


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Cliente de la Agenda")
    parser.add_argument('--port', help="puerto", type=int)
    parser.add_argument('--host', help="host", type=str)

    args = parser.parse_args()

    port = 8888
    host = '127.0.0.1'

    if args.port:
        port = args.port

    if args.host:
        host = args.host

    ThreadedServer(host, port).listen()



