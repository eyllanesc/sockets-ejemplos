#!/usr/bin/env python
# -*- coding: utf-8 -*-

import select
import socket
import sys

from config import AHORCADO

HOST = ''

RECV_BUFFER = 4096
PORT = 9009

MAX_CLIENTS = 2


def update_word(word, letters):
    new_word = ""
    for letter in word:
        if letter in letters:
            new_word += letter
        else:
            new_word += "_"
    return new_word


class GameServer:
    def __init__(self, host, port):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind((host, port))
        self.server_socket.listen(10)
        self.SOCKET_LIST = []

        self.some_letters = ['a', 'e', 'i', 'o', 'u']

        self.current_player = self.another_player = None

        # add server socket object to the list of readable connections
        self.SOCKET_LIST.append(self.server_socket)

        print "La sala de juego inicio en el puerto " + str(PORT)

        self.palabra = None
        self.number_of_attemps = 0

        self.secret_word_mod = ""

    def reset(self):
        self.palabra = None
        self.number_of_attemps = 0
        self.secret_word_mod = ""
        self.some_letters = ['a', 'e', 'i', 'o', 'u']

    def run(self):

        while True:
            ready_to_read, ready_to_write, in_error = select.select(self.SOCKET_LIST, [], [], 0)

            for sock in ready_to_read:
                if sock == self.server_socket:
                    sockfd, addr = self.server_socket.accept()
                    if len(self.SOCKET_LIST) <= MAX_CLIENTS:
                        self.SOCKET_LIST.append(sockfd)
                        print "Client (%s, %s) connected" % addr

                        self.broadcast(sockfd, "[%s:%s] entered our chatting room\n" % addr)
                        sockfd.send("Eres el jugador número {}\n".format(len(self.SOCKET_LIST) - 1))

                        if len(self.SOCKET_LIST) == (MAX_CLIENTS + 1):
                            _, self.current_player, self.another_player = self.SOCKET_LIST
                            self.current_player.send("Ingrese una palabra de al menos 5 letras: \n")
                    else:
                        sockfd.send("Esta llena la sala, suerte para la proxima\n")
                        sockfd.close()

                else:
                    try:
                        data = sock.recv(RECV_BUFFER).strip()
                        if data:
                            if sock == self.current_player:
                                if not self.palabra:
                                    if not data.isalpha() or len(data) < 5:
                                        sock.send("Ingrese una palabra que contenga solo letras por favor\n")
                                    else:
                                        self.palabra = data.lower()
                                        sock.send("la palabra es {}\n".format(self.palabra))
                                        self.secret_word_mod = update_word(self.palabra, self.some_letters)
                                        self.broadcast(message="Letras utilizadas:\n{}\n"
                                                       .format(self.secret_word_mod))
                                        self.another_player.send("Ingrese una letra\n")
                                else:
                                    sock.send("Espera tu turno\n")
                            elif sock == self.another_player:
                                if self.palabra:
                                    letter = data.lower()
                                    self.another_player.send("Ingreso la letra {}\n".format(letter))
                                    if not letter.isalpha():
                                        sock.send("Ingrese una letra\n")
                                    elif len(letter) != 1:
                                        sock.send("Introduce una sola letra\n")
                                    elif letter in self.some_letters:
                                        sock.send("Ya has elegido esa letra, elige otra\n")
                                    else:
                                        self.some_letters.append(letter)
                                        secret_word_mod_temp = update_word(self.palabra, self.some_letters)
                                        if self.secret_word_mod == secret_word_mod_temp:
                                            self.number_of_attemps += 1
                                            self.broadcast(message=AHORCADO[self.number_of_attemps - 1]+"\n")
                                        else:
                                            self.secret_word_mod = secret_word_mod_temp

                                        self.broadcast(message="Letras utilizadas:\n{}\n"
                                                       .format(self.secret_word_mod))

                                        if self.secret_word_mod == self.palabra:
                                            self.another_player.send("Ganaste\n")
                                            self.current_player.send("Ingresa una nueva palabra de al menos 5 letras\n")
                                            self.reset()

                                        if self.number_of_attemps == len(AHORCADO):
                                            self.another_player.send("perdiste\n")
                                            tmp = self.current_player
                                            self.current_player = self.another_player
                                            self.another_player = tmp
                                            self.current_player.send("Ingresa una nueva palabra de al menos 5 letras\n")

                                            self.reset()

                                else:
                                    sock.send("Espera tu turno\n")
                        else:
                            # remove the socket that's broken
                            if sock in self.SOCKET_LIST:
                                self.SOCKET_LIST.remove(sock)

                            # at this stage, no data means probably the connection has been broken
                            self.broadcast(sock, "Client (%s, %s) se desconecto\n" % addr)

                            # exception
                    except socket.error, exc:
                        self.broadcast(sock, "Client (%s, %s) se desconecto\n" % addr)
                        continue

        self.server_socket.close()

    def broadcast(self, sock=None, message=""):
        for socket in self.SOCKET_LIST:
            # send the message only to peer
            if socket != self.server_socket and socket != sock:
                try:
                    socket.send(message)
                except:
                    # broken socket connection
                    socket.close()
                    # broken socket, remove it
                    if socket in self.SOCKET_LIST:
                        self.SOCKET_LIST.remove(socket)


if __name__ == "__main__":
    server = GameServer(port=9009, host="")
    sys.exit(server.run())
